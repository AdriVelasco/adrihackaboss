const people = {
    Iria: 30,
    Ana: 14,
    Luis: 16,
    Pepe: 35,
    Manuel: 50,
    Teresa: 12,
    Daniel: 27,
    Irene: 23,
    Alex: 10,
  };
 
function edades(){
    for (const property in people) {
        if(people[property]>=18){
        console.log(`${property}` +' es mayor de edad');
      }else
      console.log(`${property}` +' es menor de edad');
}
}
edades();